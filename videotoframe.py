import cv2 as cv
import os
import time

datafolder = os.getcwd()+'/videofiles/'

files = os.listdir(datafolder)

for index,file in enumerate(files):
    path = datafolder+file
    cap = cv.VideoCapture(path)
    try:
        if not os.path.exists('frames'):
            os.makedirs('frames')
    except OSError:
        print('Error: Creating directory of data')

    currentFrame = 0
    while (True):
        # Capture frame-by-frame
        ret, unresized_frame = cap.read()
        r = 500.0 / unresized_frame.shape[1]
        dim = (500, int(unresized_frame.shape[0] * r))

        # perform the actual resizing of the image and show it
        frame = cv.resize(unresized_frame, dim, interpolation=cv.INTER_AREA)

        print(frame.shape)
        if not ret:
            print("here")
            break
        # Saves image of the current frame in jpg file
        name = './frames/'+'_'+str(index)+'_'+ str(currentFrame) + '.jpg'
        print('Creating...' + name)
        cv.imwrite(name, frame)

        # To stop duplicate images
        currentFrame += 1
        if(currentFrame > 200):
            break

    cap.release()
    # cv.destroyAllWindows()


